import varieties from './index';

test('the data not empty', done => {
  function callback(error, data) {
    expect(data).toHaveLength(14);
    expect(error).toBeNull();
    done();
  }

  varieties.getData(callback);
});