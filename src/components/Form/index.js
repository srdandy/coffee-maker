import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from '../Select';
import Countries from '../Countries';
import { uniqBy, uniq } from 'lodash';
import './Form.css';

export default class Form extends Component{
  static propTypes = {
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        land: PropTypes.string.isRequired,
        variety: PropTypes.array.isRequired,
        img: PropTypes.string.isRequired,
      })
    ),
  }
  
  constructor(props) {
    super(props);
    this.state = { 
      regions: null,
      countries: null,
      varieties: null,
      availableCountries: null,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  // On each select change, update all the data for the options
  handleChange = (e) => {
    const valueType = e.target.name;
    const value = e.target.value;
    this.filterData(valueType, value);
  }

  componentDidUpdate = (prevProps) => {
    const { data } = this.props;
    // Check if we really need the change, otherwise don't update anything
    if (data !== prevProps.data) {
      this.formatOptions(data);
    }
  }

  filterData(valueType, value){
    const { data } = this.props;
    const newData = data.filter((item) => {
                      if(valueType !== 'variety') return item[valueType] === value;
                      return item[valueType].indexOf(value) !== -1;
                    });
    const countries = uniqBy(newData.map((item) => ({ land: item.land, img: item.img })), 'land');
    this.setState({ availableCountries: countries });
    this.formatOptions(newData);
  }

  formatOptions(data) {
    const regions = uniqBy(data.map((item) => ({ name: item.region })), 'name');
    const countries = uniqBy(data.map((item) => ({ name: item.land })), 'name');
    const varieties =  uniq(data.map((item) => (item.variety))
                      .reduce((prev, curr) => { 
                        return prev.concat(curr);
                      })).map((item) => ({ name: item }));

    this.setState({
      region: regions,
      land: countries,
      variety: varieties
    });
  }
  

  render() {
    const { region, land, variety, availableCountries } = this.state;
    return (
      <div className="row">
        <div className="col-xs-12 col-md-8">
          <form className="cf-form">
            <label className="cf_label">
              <span className="cf_span">Region:</span>
              <Select
                name="region"
                options={region}
                onChange={this.handleChange}
              />
            </label>
            <label className="cf_label">
            <span className="cf_span">Country:</span>
              <Select
                name="land"
                options={land}
                onChange={this.handleChange}
              />
            </label>
            <label className="cf_label">
            <span className="cf_span">Variety:</span>
              <Select
                name="variety"
                options={variety}
                onChange={this.handleChange}
              />
            </label>
          </form>
        </div>
        <div className="col-xs-12 col-md-4">
          {
            availableCountries ?
            <Countries flags={availableCountries} />
            : null
          }
        </div>
      </div>
    )
  }
}