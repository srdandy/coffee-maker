import React from 'react';
import Form from './index';
import renderer from 'react-test-renderer';
import varieties from '../../service/index';

it('form renders correctly without data', () => {
  const tree = renderer.create(
    <Form data={[]} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

it('form renders correctly with data', () => {
  varieties.getData((err, data) => {
    if(!err) {
      const tree = renderer.create(
        <Form data={data} />
      ).toJSON();
      expect(tree).toMatchSnapshot();
    }
  });
});