import React from 'react';
import PropTypes from 'prop-types';
import './Countries.css';

const Countries = ({ flags }) => (
  <div>
    <h4>Available in this countries</h4>
    <ul className="cf-ul">
      {
        flags.map((item, index) => (
          <li className="cf-list" key={`${item.land}-${index}`}>
            <img src={item.img} alt={item.land} className="cf-img" />
            <p className="cf-description">{item.land}</p>
          </li>
        ))
      }
    </ul>
  </div>
);

Countries.propTypes = {
  flags: PropTypes.arrayOf(
    PropTypes.shape({
      land: PropTypes.string.isRequired,
      img: PropTypes.string.isRequired,
    })
  ),

};

export default Countries;
