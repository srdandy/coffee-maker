import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Select.css';

export default class Select extends Component{
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
      })
    ),
    name: PropTypes.string,
    onChange: PropTypes.func,
  }
  
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e){
    this.setState({ value: e.target.value });
    if(this.props.onChange) this.props.onChange(e);
  }

  render() {
    const { options, name } = this.props;
    return (
      <select 
        name={name} 
        value={this.state.value} 
        onChange={this.handleChange}
        className="cf-select"
      >
        {
          !options ?
          <option>Loading... </option>
          : options.map((item, index) => (<option key={`${item}-${index}`}>{item.name}</option>))
        }
      </select>
    )
  }
}