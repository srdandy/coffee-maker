import React, { Component } from 'react';
import Form from './components/Form';
import varieties from './service';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { coffeData: null, error: null };
  }

  componentDidMount = () => {
    varieties.getData((err, data) => {
      if(!err) this.setState({ coffeData: data, error: null });
      this.setState({ error: err });
    });
  }

  render() {
    const { coffeData, error } = this.state;
    return (
      <main>
        <div className="container-fluid cf-header">
          <div className="row">
            <h1 className="cf-h1">Coffee Variety.</h1>
            <h2>☕</h2>
          </div>
        </div>
        <div className="container">
          {
            !error ?
            <Form data={coffeData} />
            : <h3>Oops, something went wrong! ☕. Try again later</h3>
          }
        </div>
      </main>
    );
  }
}

export default App;
